<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
        $this->load->helper('url_helper');
		$this->load->library('form_validation');
		$this->load->model('User_model'); 
		$this->load->model('Jadwal_model');
	}

	public function index(){
		$data['judul'] = "Bimbingan Online | Halaman Utama";
		$data['user'] = $this->db->get_where('user', ['username' =>
		$this->session->userdata('username')])->row_array();
		$this->load->view('templates/header', $data);
		$this->load->view('mahasiswa/index', $data);
		$this->load->view('templates/footer', $data);
	}

	public function tambah_jadwal($id_user){
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('nim', 'Nim', 'trim|required');
		$data['judul'] = "Bimbingan Online | Halaman Utama";
		$data['user'] = $this->db->get_where('user', ['username' =>
		$this->session->userdata('username')])->row_array();
		$data['dosen'] = $this->User_model->getDosen($id_user);
		$data['jadwal'] = $this->Jadwal_model->getJadwal2($id_user);
		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('mahasiswa/tambah_jadwal', $data);
			$this->load->view('templates/footer', $data);
		}else{
			if (isset($_POST["simpan"])) {
			$data = [
				"nama" => $this->input->post('nama', true),
				"nim" => $this->input->post('nim', true),
				"angkatan" => $this->input->post('angkatan', true),
				"id_dosen" => $this->input->post('id_dosen', true),
				"id_user" => $this->input->post('id_user', true),
				"waktu" => $this->input->post('waktu', true),
				"topik" => $this->input->post('topik', true),
				"ket" => $this->input->post('ket', true),
				"status" => "Menunggu persetujuan" 
			];
			//1. menyimpan data ke tabel pembelian
			$this->db->insert('bimbingan', $data);
			redirect('mahasiswa/lihat_jadwal');
		}
	}
}

	public function detail_jadwal($id_bimbingan){
		$id_user = $this->session->userdata('id_user');
		$data['judul'] = "Bimbingan Online | Halaman Utama";
		$data['user'] = $this->db->get_where('user', ['username' =>
		$this->session->userdata('username')])->row_array();
		$data['bimbingan'] = $this->Jadwal_model->getBimbingan4($id_bimbingan);
		$this->load->view('templates/header', $data);
		$this->load->view('mahasiswa/detail_jadwal', $data);
		$this->load->view('templates/footer', $data);
	}

	public function lihat_jadwal(){
		$id_user = $this->session->userdata('id_user');
		$data['judul'] = "Bimbingan Online | Halaman Utama";
		$data['user'] = $this->db->get_where('user', ['username' =>
		$this->session->userdata('username')])->row_array();
		$data['bimbingan'] = $this->Jadwal_model->getBimbingan($id_user);
		$this->load->view('templates/header', $data);
		$this->load->view('mahasiswa/lihat_jadwal', $data);
		$this->load->view('templates/footer', $data);
	}

	public function pilih_dosen(){
		$data['judul'] = "Bimbingan Online | Halaman Utama";
		$data['user'] = $this->db->get_where('user', ['username' =>
		$this->session->userdata('username')])->row_array();
		$data['dosen'] = $this->User_model->getDosen2();
		$this->load->view('templates/header', $data);
		$this->load->view('mahasiswa/pilih_dosen', $data);
		$this->load->view('templates/footer', $data);
	}
	public function delete($id_bimbingan)
    {
        $this->db->where('id_bimbingan', $id_bimbingan); // bisa menggunakan ini
        $this->db->delete('bimbingan', ['id_bimbingan' => $id_bimbingan]);
    }
}